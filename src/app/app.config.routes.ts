import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './components/home.component';
import { RegisterProduct } from './components/registerProduct.component'

const appRoutes: Routes = [
    {
        path: "",
        component: HomeComponent
    },
    {
        path: "home",
        component: HomeComponent
    },
    {
     path: 'registro',
     component: RegisterProduct   
    }
    ,
    {
     path: '**',
     component: HomeComponent   
    },
    {
        path: 'menu3',
        component: HomeComponent   
       }
];
export const appRoutingProvider: any[]=[];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

