import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { routing, appRoutingProvider }  from  './app.config.routes'

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home.component';
import { RegisterProduct } from './components/registerProduct.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterProduct
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing
  ],
  providers: [appRoutingProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
