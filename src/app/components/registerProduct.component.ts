import { Component } from '@angular/core';
import { ProductForm } from '../Forms/ProductForm';

@Component({
    selector: 'register-product',
    templateUrl: '../views/registerProduct.view.component.html'
})

export class RegisterProduct{
    private title = "Registrar Producto";
    public product:ProductForm = new ProductForm();;
    public step:number = 1;

    constructor(){
    }      
    
    nextStep(){        
        if(this.step < 3){
            this.step ++;
        }
    }
    backStep(){
        if(this.step > 1){
            this.step --;
        }
    }
    resetForm(){
        this.step = 1;
        this.product = new ProductForm();  
    }
}